

## Insertar al final de la líne
para ello es ```A```

## Insertar al comienzo de la línea
para ello es ```I```

## volver a la definicion
lo que hace es que al estar en una palabra (variable, funición, método) o parecido
al precionar ```gd``` este salta en regreso a su definición
```
const{ definicion } = require('esta sería la deficion')

definicion(){
	return "esta función obtuvo la defición al situar el cursos en la palabra definición y después gd , este regresará a la anterior, podría ser que reconoce lenguaje por la extención"
}
```

## ir al la definición o llamada de un archivo aparte
Esto permite saltar de un archivo a otro con ```gf```
```
//helper.js
module.exports.ayuda = () => {
	Console.log('lala')
}


// index.js
const{ ayuda } = require(./helper.js)

definicion(){
	return "esta función obtuvo la defición al situar el cursos en la palabra definición y después gd , este regresará a la anterior, podría ser que reconoce lenguaje por la extención"
}
```
## viajar entre historial

realizar esos saltos entre definiciones y archivos genera un historial y para viajar entre ese historias se usa:
* ir a atrás del historial ```ctrl + o```
* ir a adelante del historial ```ctrl + i```

## deshacer y rehacer cambios

* undo : ```u```
* redo : ```ctrl + r```

## Eliminar
para eliminar se usa combinaciones de posiciones y movimientos combinada con ```d```

* ```wd``` : elimina hasta antes del comienzo de la siguiente palabra
* ```dd``` : elimina toda la línea
* ```d$``` : situado desde el inicio de la líne ( ```0``` ) elimina todo el contenido de la línea sin eliminar la línea en sí.
* etc

## Pegar
Eliminar con ```d``` hace que este se quede en un clipboard con la que podemos pegar lo que se encuentre ahí 
* ```p``` (en caso haymos hecho ```dd```) pegará la línea debajo de la línea en la que se encuentra el cursor.
* ```P``` (el mismo caso) Pegará la línea encima de la línea en la que se encuentra el cursor


## comando para reemplazar ()
```r``` + caracter por la que se desee reemplazar.
```R``` modo reemplazar. similar a modo insertar, pero esta reemplaza los caracteres.

## Operador de Cambio
* 1ero: ```cw``` change word elimina parte de la palabra desde donde se encuentra el cursor y se pone en modo insertar
* 2do: ```ciw```change inner word elimina la palabra aunque el cursor se ubique en medio y después pasa a modo insertar para que hagas el cambio.


## SAltos de línea
```gg```   ```G``` ```16G``` ```16gg```

## Busquedas 
```?``` : busqueda hacie arriba desde el cursor
```/``` : busqueda hacia anajo desde el cursor
-> se usa la ```n``` y ```N``` para viajar entre las busquedas


## viajar entre los llaves, parentesis , corchetes
```%```

## substituir 

```:s/palabraASustituir/palabraDeReemplazo``` : solo la primera coincidencia
```:s/palabraASustituir/palabraDeReemplazo/g``` : todas las coincidencias solo dentro de la línea
```:%s/palabraASustituir/palabraDeReemplazo/g``` : En todo el archivo sin preguntar
```:%s/palabraASustituir/palabraDeReemplazo/gc``` : En todo el archivo, pero preguntando por cada uno


