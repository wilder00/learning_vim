
## Configurando Vim

### para neoVIM
```
cd ~
cd .config/nvim/
nvim init.vim
```
asegurarse de que las siguientes lineas se encuentran en el archivo

```
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath=&runtimepath
source ~/.vimrc
```

Lo que hace es que neovim pueda usar el mismo archivo de configuración que vim

### para vim

Debería ser necesario ver la ubicación de los archivos. ya que lo pongo es porque uso gitbash
No necesarioamente el archivo .vimrc debe existir, la  creamos en esa dirección

```
vim ~/.vimrc
```
poner lo siguiente

```
set number
"para interactuar con el mouse dentro de vim
set mouse=a
"cambiamos el ancho de los números
set numberwidth=1
"para que se pueda aplicar el clipboar de vim con el que ya conocemos con el ctrl+c
set clipboard=unnamed
"syntax enable permitirá resaltado de colores
syntax enable
"para que nos muestre los comandos que vayamos ejecutanto
set showcmd
" nos mostrará la posición del cursor de vim fila y columna
set ruler
" settear la codificación
set encoding=utf-8
" para que nos muestre el parentesis de cierre
set showmatch
"para que idente con espacios en vez de con tabs, en este caso será con 2 espacios
set sw=2
"para que los números aparezcan en relación al cursor de vim, este siempre ocupará la posicíon cero 0, los demas cambiarán para adaptarse
set relativenumber


" hacemos que la barra de abajo de vim siempre se vea
set laststatus=2
" deja de mostrar el modo de vim (Insertar, Reemplazar)
set noshowmode


"##Las mias
" en vez de sw=2
" yo habria puesto set tabstop=4
"set tabstop=4

```
> una configuración curiosa (enlace)[https://geekland.eu/instalar-y-configurar-vim/]


## Instalar el gestor de extenciones
El [enlace](https://github.com/junegunn/vim-plug) llevará al gestor de extenciones para vim con las instrucciones para su instalación, pero en resumen, con un `PlugInstall` seria suficiente, sin embargo,
se necesitara installar el gestor de paquetes



```
"  En el mismo archivo de configuracioens de vim se añaden las extenciones ~/.vimrc

" insertandole tema gruvbox
" Los pluigns que instalemos se iran a buscar en la direccion asignada
call plug#begin('~/.vim/plugged')

" TEMAS
Plug 'morhetz/gruvbox'


"IDE:

"	easy motion
Plug 'easymotion/vim-easymotion'
"	scrooloose
Plug 'scrooloose/nerdtree'
"	para saltar de ventanas
Plug 'christoomey/vim-tmux-navigator'


call plug#end()

" despues de esta configuración correr :PlugInstall en vim
"Ahora sí las configuraciones del plugin
colorscheme grovbox
"contraste alto
let g:gruvbox_contrast_dark = "hard"
"let g:deoplete#enable_at_startup = 1
"let g:jsx_ext_required = 0
"para que cierre nertree cuando se selecciones el archivo
let NERDTreeQuitOnOpen=1

let mapleader=" "
"indica que el atajo solo funciona en modo normal
nmap <Leader>s <Plug>(easymotion-s2)
" CR es de enter
nmap <Leader>nt :NERDTreeFind<CR>

"nuestros propios atajos

nmap <Leader>w :w<CR>
nmap <Leader>q :q<CR>	
"nmap <Leader>e <ESC>

"esc in insertmode
inoremap hj <esc>

" esc in command mode
cnoremap hj <C-C>
" Note: In command mode mappings to esc run the command for some odd
" historical vi compatibility reason. We use the alternate method of
" existing which is Ctrl-C 

```

### Nerdtree

* ```m``` para poder crear archivos y ver los menus
* enter : para selecionar el archivo
* ```s``` : para que abra el archivo en la misma mitad de pantalla

### Vim-Tmux-navigator
mantener presionado ctrl y con hjkl se puede navegar entre ventanas de consola vim


## Datos

### ver la lista de todos los archivos instalados por vim
```
:scriptnames
```
